<?php

namespace Df\MfcIntegration\Classes\Adapters\QSystems\Types;

use Df\Base\Classes\Types\BaseType;
/**
 * Class Item
 * @package Df\MfcIntegration\Classes\QSystems\Types
 * @property-read string      $id
 * @property-read string|null $name
 * @property-read string|null $address
 * @property-read array       $phones
 * @property-read string|null $email
 *
 */
class Item extends BaseType implements ItemTypeContract
{
    /**
     * @param array $attributes
     *
     * @return array
     */
    public function parse(array $attributes): array
    {
        return [
            'id' => Arr::get($attributes, 'id'),
            'name' => Arr::get($attributes, 'name'),
            'address' => $this->parseAddress($attributes),
            'phones' => $this->parsePhones($attributes),
            'email' => Arr::get($attributes, 'email'),
        ];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getParentUid(): ?string
    {
        return null;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getAltName(): ?string
    {
        return null;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return null;
    }

    /**
     * @return bool
     */
    public function getCanCreateAppointment(): bool
    {
        return true;
    }

    /**
     * @return null|ManagerContract
     */
    public function getManager(): ?ManagerContract
    {
        return null;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @return array
     */
    public function getPhones(): array
    {
        return $this->phones;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return null;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getWindowCount(): int
    {
        return 1;
    }

    /**
     * @return string
     */
    public function getTin(): string
    {
        return null;
    }

    /**
     * @return Collection
     */
    public function getSchedule(): Collection
    {
        return new Collection;
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return null;
    }

    /**
     * @param array $attributes
     *
     * @return array
     */
    private function parsePhones(array $attributes): array
    {
        $phone = Arr::get($attributes, 'phone');

        if (empty($phone)) {
            return [];
        }

        return [formattedPhone($phone)];
    }

    /**
     * @param array $attributes
     *
     * @return string|null
     */
    private function parseAddress(array $attributes): ?string
    {
        $addressArray = array_filter([
            Arr::get($attributes, 'addressState'),
            Arr::get($attributes, 'addressCity'),
            Arr::get($attributes, 'addressLine1'),
            Arr::get($attributes, 'addressLine2'),
        ]);

        if (!count($addressArray)) {
            return null;
        }

        return implode(', ', $addressArray);
    }

    /**
     * @return string|null
     */
    public function getLat(): ?float
    {
        return null;
    }

    /**
     * @return string|null
     */
    public function getLon(): ?float
    {
        return null;
    }
}
