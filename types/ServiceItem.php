<?php

namespace Df\MfcIntegration\Classes\Adapters\QSystems\Types;

use Df\Base\Classes\Types\BaseType;
use Df\MfcIntegration\Classes\Contracts\Types\ServiceItem as ServiceItemTypeContract;
use Illuminate\Support\Arr;

/**
 * Class ServiceItem
 * @package Df\MfcIntegration\Classes\QSystems\Types
 * @property-read string      $id
 * @property-read string|null $name
 * @property-read array       $mfc_list
 * @property-read array       $groups_list
 */
class ServiceItem extends BaseType implements ServiceItemTypeContract
{
    /**
     * @param array $attributes
     *
     * @return array
     */
    public function parse(array $attributes): array
    {
        return [
            'id' => Arr::get($attributes, 'id'),
            'name' => Arr::get($attributes, 'name'),
            'mfc_list' => Arr::get($attributes, 'mfc_list', [])
        ];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEtag(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return '';
    }

    /**
     * @return string|null
     */
    public function getServiceTypeId(): ?string
    {
        return null;
    }

    /**
     * @return string
     */
    public function getCategoryId(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getDepartmentId(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getCustomerType(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getLifeSituationId(): string
    {
        return '';
    }

    /**
     * @return null|string
     */
    public function getRegulatoryDocuments(): ?string
    {
        return null;
    }

    /**
     * @return array
     */
    public function getNormativeDocuments(): array
    {
        return [];
    }

    /**
     * @return null|string
     */
    public function getExternalQueueCode(): ?string
    {
        return null;
    }

    /**
     * @return array
     */
    public function getQuestionnaire(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function getMfcList(): array
    {
        return $this->mfc_list;
    }

    /**
     * @return string|null
     */
    public function getServiceResult(): ?string
    {
        return null;
    }

    /**
     * @return string|null
     */
    public function getExecutePeriod(): ?string
    {
        return null;
    }

    /**
     * @return int
     */
    public function getRating(): ?int
    {
        return null;
    }
}
