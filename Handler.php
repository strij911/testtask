<?php

namespace Df\MfcIntegration\Classes\Adapters\QSystems;

use Df\MfcIntegration\Classes\Adapters\QSystems\Types\Item;
use Df\MfcIntegration\Classes\Adapters\QSystems\Types\ServiceItem;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Collection;

class Handler implements AisHandler
{

    /**
     * @return Collection
     *
     * @throws ErrorConnectingIntegrationException
     * @throws GuzzleException
     */
    public function getMfcList(): Collection
    {
        return $this->loadMfcList()->mapInto(Item::class);
    }

    /**
     * @return Collection
     *
     * @throws ErrorConnectingIntegrationException
     * @throws GuzzleException
     */
    public function getServices(): Collection
    {
        return $this->loadServicesList()->mapInto(ServiceItem::class);
    }

    /**
     * @return Collection
     *
     * @throws ErrorConnectingIntegrationException
     * @throws GuzzleException
     */
    private function loadMfcList(): Collection
    {
        return new Collection($this->get(static::BRANCHES_BASE_URI));
    }

    /**
     * @return Collection
     *
     * @throws ErrorConnectingIntegrationException
     * @throws GuzzleException
     */
    private function loadServicesList(): Collection
    {
       // Здесь может быть ваш код
    }

   /**
     * @param string $mfcUuid
     *
     * @return Collection
     *
     * @throws ErrorConnectingIntegrationException
     * @throws GuzzleException
     */
    private function loadMfcServices(string $mfcUuid): Collection
    {
        return new Collection(Arr::get($this->get(static::BRANCHES_BASE_URI . "/$mfcUuid/services"), 'MenuItems', []));
    }

}
